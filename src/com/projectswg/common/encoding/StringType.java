package com.projectswg.common.encoding;

public enum StringType {
	UNSPECIFIED,
	ASCII,
	UNICODE
}
